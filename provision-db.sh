# enable console colors
sed -i '1iforce_color_prompt=yes' ~/.bashrc

# Edit the following to change the version of PostgreSQL that is installed
PG_VERSION=9.3

sudo apt-get update

sudo apt-get -y install "postgresql-$PG_VERSION" "postgresql-server-dev-$PG_VERSION" "postgresql-contrib-$PG_VERSION"

sudo service postgresql stop

PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
PG_DIR="/var/lib/postgresql/$PG_VERSION/main"

# Edit postgresql.conf to change listen address to '*':
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"

# Edit postgresql.conf to listen on different port than default
sudo sed -i "s/port = 5432/port = 50603/" "$PG_CONF"

# Append to pg_hba.conf to add password auth:
sudo sed -i "/IPv4 local connections:/a host    all             all             all                     md5" "$PG_HBA"

# Restart so that all new config is loaded:
sudo service postgresql start

# Create pgadmin user 
sudo sudo -u postgres psql -1 -c "CREATE ROLE pgadmin WITH SUPERUSER CREATEDB INHERIT LOGIN ENCRYPTED PASSWORD 'password';"

# Allow administrator role to connecto the database
sudo sudo -u postgres psql -1 -c "GRANT CONNECT ON DATABASE postgres TO pgadmin;"