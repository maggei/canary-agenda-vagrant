# enable console colors
sed -i '1iforce_color_prompt=yes' ~/.bashrc

# Install Git
sudo apt-get install -y git

# Install PPA (Personal Package Archive) from nodesource.com
curl -sL https://deb.nodesource.com/setup | sudo bash -

# Installing node.js and npm
sudo apt-get -y install nodejs

# Install build-essential package for npm
sudo apt-get -y install build-essential

# Install Heroku Toolbelt
wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh

# For DEVELOPMENT ONLY. Automatically restart node server when changes occur. 
sudo npm install -g nodemon