# Vagrant machines for Canary-Agenda
This project is a web application to manage agenda items.

## Getting started
To setup these machines you'll need the following installed in your computer

+ Git (1.9.5 For Windows, 2.4.4 For OSX)
+ VirtualBox (4.3.28)
+ Vagrant (1.7.2)

*The following instructions assume you have setup similar projects in the past and skips into many of the details required to get the machines working. If you are not sure this is your case, better go to Detailed Steps down this page.*

Once you have the previous software installed locally in your computer, run the
following commands to get the servers going

*Retrieve the machines configuration from bitbucket*
```
git clone git@bitbucket.org:maggei/canary-agenda-vagrant.git
```
*Change to root folder of the project*
```
cd canary-agenda-vagrant
```

*Create the following folders*
```
mkdir projects
mkdir projects/web
mkdir projects/api
```

*Start virtual machines*
```
vagrant box update // Optional
vagrant up
```

*Verify virtual machines are running*
```
vagrant status
```

*ssh into each of the machines*
```
vagrant ssh web
vagrant ssh api
vagrant ssh db
```

## Folder Structure
```
| .gitignore
| provision-api-docs.sh
| provision-api.sh
| provision-db.sh
| provision-web.sh
| readme.md
| Vagrantfile
```

### Files
**.gitignore**
The instructions to ignore files for git

**provision-api-docs.sh**
All the shell instructions to provision a web server hosting the API documentation

**provision-api.sh**
All the shell instructions to provision a server hosting the REST API

**provision-db.sh**
All the shell instructions to provision a Postgresql database server

**provision-web.sh**
All the shell instructions to provision the web server

**readme.md**
This file

**Vagrantfile**
The main vagrant file, defining each of the server's configuration. Operative system, IP addresses, port mapping, etc...

# Detailed Steps
As every application depends on multiple parts working together, those different parts will be running from virtual machines trying to emulate the production environment as closely as possible.

Setting up the virtual machines to properly work in the development environment is a key element to good development.

## The overall picture
For this particular web application we'll have several major components described here.

### Web server
This is the web server hosting the application and rendering the single page application.

### Database Server
This is a Postgresql database server storing all the information relevant to the web application. This is a transactional-relational engine that needs to be running for the application to function.

### API Server
The API server exposes methods and messages to other applications that may invoke these methods remotely. In general terms the API server gets requests through HTTP and ultimately makes call to the database server. Once the operations are performed the API server returns messages to its clients

### API Docs Server
This server hosts all the documentation related to the API. It is here for the purpose of validating the documentation created or modified properly renders in the web server since all the documentation is created with apiblueprint format

## The set up
Here is the sequence of steps to set up your local development environment. These steps need to be followed in the same sequence described here.

*One important note here. None of the services setup here runs on the default ports. Please don't forget to have this into consideration when doing development or testing

+ Node.js for WEB server. Listening on 58000
+ Postgresql. Listening on 55432
+ Nginx. Listening on 50080
+ Node.js for API server. Listening on 53000*

### Setup Bitbucket
In order to setup the virtual machines you will need access to Bitbucket. The first step to get access to Bitbucket is to have an account.

If you don’t have an account yet with Bitbucket, please go ahead and open an account. [BitBucket signup](https://bitbucket.org/account/signup/)

If you already have a Bitbucket account or you just created a new one by following the previous step you’ll need to make sure you have a defined ssh identity for git. If you have an ssh identity already you may skip the following section and go directly to Request access to BitBucket Repository

### Creating ssh identity for git
Follow steps 1 though 6 described [here](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git)

### Request access to BitBucket Repository
Send email to mparra@crowdof5.com requesting access to BitBucket. Please include the following information

```
Subject. Requesting access to BitBucket - Canary-Agenda project
Message.
Your name: your-first-name your-last-name
BitBucket username: your-username-here
BitBucket repository: git@bitbucket.org:maggei/canary-agenda-vagrant.git
```

**Get the vagrant machines from BitBucket**

Once you have access to git@bitbucket.org:maggei/canary-agenda-vagrant.git] clone the project in your local computer

```
git clone git@bitbucket.org:maggei/canary-agenda-vagrant.git
```

**Create The Necessary Folders**

When getting the repository from Bitbucket it will create a folder named canary-agenda-vagrant. Move into that folder and create a new projects folder. Create web, api and api-docs folders inside projects.

```
cd canary-agenda-vagrant
mkdir projects
mkdir projects/web
mkdir projects/api
```

**Start Virtual Machines**

In this step you’ll want to go and grab some coffee since this process make take a while depending on the speed of your internet connection. You’ll be downloading images with ubuntu 14.04 into your local computer and automatically setting up 3 different servers. One acting as a web server, one acting as a database server, one as REST API server and one as a web server hosting API documentation.

You’ll want to start the machines from the canary-agenda-vagrant folder

```
cd canary-agenda-vagrant
vagrant box update // Optional
vagrant up
```

**Test the servers are running**
Once the previous step completes you need to check the 4 machines are running. When running the vagrant command all 4 machines should have an status of running. The 4 machines running are web, api, api-docs and db

```
vagrant status
```

**SSH into each machine**
You now need to verify you can actually establish an ssh session with each of the machines. For each of the machines you should be able to login into them without the need for a username or password. You also should be able to go into the projects folder you created in one of the previous steps

*In the command line for your local computer.*
```
vagrant ssh api
```

*Now that you are inside the api machine, verify you can go to projects folder*
```
cd projects
```

*In the command line for your local computer. My suggestion is you open a second window and ssh into a second virtual machine running the database server*
```
vagrant ssh db
```

*Now that you are inside the db machine, verify postgresql is running. You should get a response where the service tell you it is online*
```
service postgresql status
```
*In the command line for your local computer. My sugestion is you open a third window and ssh into a third virtual machine running the web server*
```
vagrant ssh web
```

*Now that you are inside the web machine, verify you can go to projects folder*
```
cd projects
```

At this point you should have 4 different virtual servers running at the same time
+ web (The web server)
+ db (The database server)
+ api (The web server that will be hosting the api)

You can easily check this by running the status command and verifying the results
```
vagrant status
```