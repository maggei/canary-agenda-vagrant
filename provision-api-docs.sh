# enable console colors
sed -i '1iforce_color_prompt=yes' ~/.bashrc

sudo apt-get -y update

# Installing git
sudo apt-get install -y git

# Install PPA (Personal Package Archive) from nodesource.com
curl -sL https://deb.nodesource.com/setup | sudo bash -

# Install build-essential package for npm
sudo apt-get -y install build-essential

# Installing g++. In support of aglio
# sudo apt-get -y install g++

# Installing node.js
sudo apt-get -y install nodejs

# Installing nodemon
sudo npm install -g nodemon

# Install Heroku Toolbelt
wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh

# Installing aglio. apiblueprint converter to html
sudo npm install -g aglio

# Installing nginx
sudo apt-get -y install nginx

# Edit default to listen on different port than default
sudo sed -i "s/listen 80 default_server;/listen 50604 default_server;/" /etc/nginx/sites-enabled/default
sudo sed -i "s/:80 /:50604 /" /etc/nginx/sites-enabled/default

sudo rm -rf /usr/share/nginx/html/
sudo ln -s /home/vagrant/projects/api-docs /usr/share/nginx/html

sudo service nginx start