# enable console colors
sed -i '1iforce_color_prompt=yes' ~/.bashrc

sudo apt-get -y update

# Installing git
sudo apt-get install -y git

# Install PPA (Personal Package Archive) from nodesource.com
curl -sL https://deb.nodesource.com/setup | sudo bash -

# Installing node.js
sudo apt-get -y install nodejs

# Installing nodemon
sudo npm install -g nodemon

# Install Heroku Toolbelt
wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh

sudo apt-get -y install openjdk-7-jre-headless

# Install google chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get update
sudo apt-get -y install google-chrome-stable

# Installing protractor for integration testing
sudo npm install -g protractor
sudo webdriver-manager update