# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

# ---- Multi-machine environment --- #

Vagrant.configure("2") do |config|

  config.vm.define "web" do |web|
    # Configuring the machine with desktop version of ubuntu so we can run Karma unit tests with Chrome
    # web.vm.box = "ubuntu/trusty64"
    
    # We may decide later to configure the environment with desktop version of ubuntu so we can run Karma unit
    # tests with Chrome
    web.vm.box = "box-cutter/ubuntu1404-desktop"

    # Forward ports to host machine
    web.vm.network "forwarded_port", guest:50600, host:50600, id:"canary-agenda-angular"
    # web.vm.network "forwarded_port", guest:4444, host:54444, id:"protractor-status"

    # Install packages
    web.vm.provision "shell", path:"provision-web.sh", privileged: false

    # Use an specific IP address and hostname on local network
    web.vm.network "private_network", ip: "172.16.3.100"
    web.vm.hostname = "canary-agenda-web"

    # Share additional folders to the guest VM
    web.vm.synced_folder "./projects/canary-agenda-web", "/home/vagrant/projects/web"
  end

  # This machine will host the api with Express
  config.vm.define "api" do |api|
    api.vm.box = "ubuntu/trusty64"

    api.vm.network "forwarded_port", guest: 50601, host: 50601, id: "Express-Server"
    api.vm.network "forwarded_port", guest: 50602, host: 50602, id: "Foreman-Server"
  
    # Use an specific IP address and hostname on local network
    api.vm.network "private_network", ip: "172.16.3.101"
    api.vm.hostname = "canary-agenda-api"

    # Share additional folders to the guest VM
    api.vm.synced_folder "./projects/canary-agenda-api", "/home/vagrant/projects/api"

    api.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "1024"]
    end

    api.vm.provision :shell, path: "provision-api.sh", privileged: false
  end

  # This machine will host the database with Postgresql
  config.vm.define "db" do |db|
    db.vm.box = "ubuntu/trusty64"

    db.vm.network "forwarded_port", guest: 50603, host: 50603, id: "Postgresql-Db-Server"

    # Use an specific IP address and hostname on local network
    db.vm.network "private_network", ip: "172.16.3.102"
    db.vm.hostname = "canary-agenda-db"

    db.vm.synced_folder "./projects", "/home/vagrant/projects/"

    db.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "1024"]
    end

    db.vm.provision :shell, path: "provision-db.sh", privileged: false
  end

  # This machine will host the API documentation. It is a simple web server and has tools to # generate documentation
   config.vm.define "api-docs" do |apidocs|
     apidocs.vm.box = "ubuntu/trusty64"
   
     apidocs.vm.network "forwarded_port", guest: 50604, host: 50604, id: "api-docs"
   
     # Use an specific IP address and hostname on local network
     apidocs.vm.network "private_network", ip: "172.16.3.103"
     apidocs.vm.hostname = "canary-agenda-api-docs"
   
     apidocs.vm.synced_folder "./projects/canary-agenda-api-docs", "/home/vagrant/projects/api-docs"
     apidocs.vm.provision :shell, path: "provision-api-docs.sh", privileged: false
  end
end